# Author : Sonali Tandon (sonalitandon24@gmail.com)


from flask import Flask, render_template, request, jsonify
import json, requests, twitter, re, fastText, config, os
from flask_cors import CORS
# from langdetect import detect
from boto.s3.key import Key
from boto.s3 import connect_to_region
from boto.s3.connection import S3Connection, OrdinaryCallingFormat
import random

app = Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

api = twitter.Api(consumer_key='ULVFOWWRwPBG31JmCSk3pA9WY',
                  consumer_secret='GkpPuajWIi8OwFNHJMnKaAvLBCQcQZdiNnEViM44eqvTvAXkf7',
                  access_token_key='973403711518183425-CNAn0AQYiT074O0XyALXdU2LiJUzGSg',
                  access_token_secret='s986l8COxFydEgyOCSuHrtGRSldyunsKfZh59TRyx1tVd')

BUCKET_NAME = 'pretrained-models-test'

conn = connect_to_region('us-west-2',
                         aws_access_key_id='AKIAI4ASHN3BK3YVESDQ',
                         aws_secret_access_key='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',
                         is_secure=False,  # uncomment if you are not using ssl
                         calling_format=OrdinaryCallingFormat()
                         )
bucket = conn.get_bucket(BUCKET_NAME)
key_obj = Key(bucket)
key_obj.key = 'model_politics.bin'
model_name = 'model_politics.bin'
temp_model_path = '/tmp/' + model_name
print(config.MODELS)
print(temp_model_path)
# contents = key_obj.get_contents_to_filename(temp_model_path)
# model_boto = fastText.load_model(temp_model_path)
print('LOADED')


# Helper functions
def make_predictions(tweets):
    predicted_json = [{'tweet': tweet} for tweet in tweets]
    for model in config.MODELS:
        model_name = 'model_' + model + config.EXTENSION
        temp_model_path = '/tmp/' + model_name
        print(temp_model_path)
        key_obj.key = model_name
        try:
            loaded_model  = fastText.load_model(temp_model_path)
        except ValueError:
            contents = key_obj.get_contents_to_filename(temp_model_path)
            loaded_model = fastText.load_model(temp_model_path)

        predict_model = loaded_model.predict(tweets)[0]
        for index, item in enumerate(predicted_json):
            item[model] = int(predict_model[index][0] == '__label__removed')
            if (item[model]):
                if 'models_that_agree' in item:
                    item['models_that_agree'].append(model)
                else:
                    item['models_that_agree'] = [model]
    return predicted_json


def clean_tweets(tweets):
    cleaned_tweets = []
    for tweet in tweets:
        # convert to lowercase and remove punctuations, emojis, URL links
        tweet = re.sub(r'(\W+)|(http\S+)', " ", str(tweet))
        tweet = tweet.lower().strip()
        # check if language is English
        # if(detect(tweet) == 'en'):
        cleaned_tweets.append(tweet)
    return cleaned_tweets


def get_user(screenName):
    user = api.GetUser(None, screenName, True, True)
    return user


def get_user_timeline(screenName, tweetCount):
    statuses = api.GetUserTimeline(None, screenName, None, None, tweetCount, True, False, False)
    return statuses


def calculate_consensus_score(tweet_json):
    user_consensus_count = 0
    for item in tweet_json:
        user_consensus_count += item['tweet_score']

    # user score = mean of tweet scores
    user_consensus_score = user_consensus_count / len(tweet_json)
    return user_consensus_score


def calculate_tweet_scores(predicted_tweet_json):
    for item in predicted_tweet_json:
        if 'models_that_agree' in item:
            item['tweet_score'] = len(item['models_that_agree']) / len(config.MODELS)
        else:
            item['tweet_score'] = 0
    return predicted_tweet_json


def get_flagged_tweets(tweet_json):
    flagged_tweets = []
    for item in tweet_json:
        if (item['tweet_score'] * 100 >= config.MIN_CAP):
            flagged_tweets.append(item['tweet'])

    return flagged_tweets


# routes
@app.route('/')
def index():
    return 'Hello from Twitter-Chrome-Mod server'


@app.route('/abusivescore', methods=['GET'])
def abusive_score():
    master_json = {}
    screen_name = request.args.get('user')
    # screen_name = 'sonalitandon24'

    # set default tweet count = 200 (maximum)
    tweet_count = 200

    # get tweets on user's timeline
    response_user_timeline = get_user_timeline(screen_name, tweet_count)
    # print(response_user_timeline)
    user_timeline_tweets = [tweet.text for tweet in response_user_timeline]

    # clean user tweets
    cleaned_user_timeline_tweets = clean_tweets(user_timeline_tweets)
    # print(cleaned_user_timeline_tweets)

    # make predictions (label removed or not)
    predicted_tweets_json = make_predictions(cleaned_user_timeline_tweets)
    predicted_tweets_json = calculate_tweet_scores(predicted_tweets_json)
    master_json['predicted_tweets'] = predicted_tweets_json

    # get flagged tweets
    flagged_tweets = get_flagged_tweets(predicted_tweets_json)
    master_json['flagged_tweets'] = flagged_tweets

    # calculate consensus score
    consensus_score = calculate_consensus_score(master_json['predicted_tweets'])
    master_json['screen_name'] = screen_name
    master_json['user_consensus_score'] = consensus_score
    master_json['number_of_tweets_considered'] = len(master_json['predicted_tweets'])
    return jsonify(master_json)


@app.route('/predict', methods=['POST', 'GET'])
def predict_tweets():
    master_json = {}
    tweets = json.loads(request.args.get('tweets'))
    cleaned_tweets = clean_tweets(tweets)
    predicted_tweets_json = make_predictions(cleaned_tweets)
    predicted_tweets_json = calculate_tweet_scores(predicted_tweets_json)
    master_json['predicted_tweets'] = predicted_tweets_json
    flagged_tweets = get_flagged_tweets(predicted_tweets_json)
    master_json['flagged_tweets'] = flagged_tweets
    return jsonify(master_json)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')
