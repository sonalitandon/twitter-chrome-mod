activate_this = '/home/ubuntu/twitter-chrome-mod/tweety_venv/bin/activate_this.py'
with open(activate_this) as f:
	exec(f.read(), dict(__file__=activate_this))

import sys
import logging

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/html/twitter-chrome-mod/")

from application import app as application
